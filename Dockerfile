FROM python:3.9

Add main.py scraper.py .
Add modules/ ./modules
RUN pip install requests bs4 numpy mysql.connector

CMD ["python", "-u", "./main.py"]