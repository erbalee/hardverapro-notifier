## Hardverapro notifier

# Objective

Hardverapro notifier. Push notifications to telegram chat if new item(s) listed.

# Update:
Harverapro made some security changes so the scraper needs to send request through a tor proxy.

```
docker pull zeta0/alpine-tor:latest
docker run -d --name tor --network scraper-net -p 5566:5566 -p 2090:2090 -e tors=5 zeta0/alpine-tor
```


# Prerequisite:
Create a bot and get token for it:
- Send message to BotFather on telegram /start. Follow the guide and you will get the bot token (https://telegram.me/BotFather). 

![image](./images/bottoken.jpg)
- - Use this token in docker command TOKEN environment variable.
- Add the bot to a group chat.
- - Click the t.me/yourbot link in the bot token message. It will open the bot in telegram
- - - Click the bot name top and choose add to group
![image](./images/botaddtogroup1.jpg)

- (if it's cannot post to group maybe you will need to add permission to the bot. You can do it with send message to botfather /mybots and follow the instructions.)

# Manual usage:
## Build the image
```
cd hardverapro-notifier
docker build -t hardverapro-scraper .
```


## Create network:
```
docker network create -d bridge scraper-net
```

## Create DB container:
```
docker run -d --name mariadb-scraper --network=scraper-net -e MARIADB_ROOT_PASSWORD=asdasd -e MARIADB_DATABASE=scraper mariadb:latest

```

## Create Scraper container:
### Variables:
- name: Container name (!!! should be unique !!!)
- network: Previously created network
- URL: Hardverapro search URL (example: https://hardverapro.hu/aprok/hardver/processzor/amd/socket_am5/keres.php?stext=7600x&stcid_text=&stcid=&stmid_text=&stmid=&minprice=&maxprice=70000&cmpid_text=&cmpid=&usrid_text=&usrid=&__buying=0&__buying=1&stext_none= )
- ITEM: Item name what will be send to telegram chat (Special characters NOT allowed)
- TOKEN: Telegram bot token
- CHATID: Telegram chatID (Easy to get from URL in browser version of telegram https://web.telegram.org)
- - Click on your channel and you can find the chatID after the # sign in the URL.
- DB_HOST: Previously created MariaDB container name
- DB_USER: Previously created MariaDB username (default root)
- DB_PASS: Previously created MariaDB root password
- DB_NAME: Previously created MariaDB database
- DB_TABLE_NAME: DB table name (optional but if set it should be unique)
- SLEEP_INTERVAL: Sleep time between queries in second (Min 60 is recommend to not get banned)
```
docker run -d \
    --name=test \
    --network=scraper-net \
    -e URL="https://hardverapro.hu/aprok/hardver/processzor/amd/keres.php?stext=5800&stcid_text=&stcid=&stmid_text=&stmid=&minprice=&maxprice=&cmpid_text=&cmpid=&usrid_text=&usrid=&__buying=0&__buying=1&stext_none=" \
    -e ITEM="Ryzen 5800X" \
    -e TOKEN="yourbottoken" \
    -e CHATID="-yourchatid" \
    -e DB_HOST=mariadb-scraper \
    -e DB_USER=root \
    -e DB_PASS="asdasd" \
    -e DB_NAME=scraper \
    -e SLEEP_INTERVAL=60 \
    -e TOR_CONTAINER_NAME=tor \
    -e TOR_PORT=5566 \
    hardverapro-scrapertest
```

# Semi-automatic usage:
Edit ITEM, URL, DB_TABLE_NAME, TOKEN, CHATID in the docker-compose.yaml. (Also you can change the MARIADB_ROOT_PASSWORD=ryzen to something secure, but then you need to change in scraper block too...)
You can delete scraper block if you don't need that much or you can copy one and edit to create a scraper.
```
cd hardverapro-notifier
docker-compose up -d
```