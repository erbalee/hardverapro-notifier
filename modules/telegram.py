
import requests

def push(TOKEN, chat_id, text, ensure_ascii=False):
    url = f'https://api.telegram.org/bot{TOKEN}/sendMessage'
    payload = {
                'chat_id': chat_id,
                'parse_mode': "Markdown",
                'text': text
                }
   
    r = requests.post(url,json=payload)
    return r