
import time
import mysql.connector
import os
import random
import string

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

# Variables
SLEEP_INTERVAL=os.getenv("SLEEP_INTERVAL")
ITEM=os.getenv("ITEM")
if not os.getenv("DB_TABLE_NAME"):
    os.environ["DB_TABLE_NAME"]=ITEM.strip().replace(" ", "")+get_random_string(10)
    DB_TABLE_NAME = os.getenv("DB_TABLE_NAME")
else: DB_TABLE_NAME = os.getenv("DB_TABLE_NAME")


# Log
print("------ Environment variable -------")
print("URL=",       os.getenv("URL"))
print("ITEM=",      os.getenv("ITEM"))
print("TOKEN=",     os.getenv("TOKEN")[:5],     "...")
print("CHATID=",    os.getenv("CHATID")[:5],    "...")
print("DB_HOST=",   os.getenv("DB_HOST"))
print("DB_NAME=",   os.getenv("DB_NAME"))
print("DB_USER=",   os.getenv("DB_USER"))
if not os.getenv("DB_PASS"):
    print("DB_PASS=")
elif os.getenv("DB_PASS") == "":
    PRINT("DB_PASS=")
else:
    print("DB_PASS=",   os.getenv("DB_PASS")[:5],   "...")
print("DB_TABLE_NAME=", DB_TABLE_NAME)
print("SLEEP_INTERVAL", os.getenv("SLEEP_INTERVAL"))
print("------ / Environment variable -------")
# DB Connect
mydb = mysql.connector.connect(
  host=os.getenv("DB_HOST"),
  user=os.getenv("DB_USER"),
  password=os.getenv("DB_PASS"),
  database=os.getenv("DB_NAME")
)

cursor = mydb.cursor()

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


# Create table if not exists
sql="""CREATE TABLE IF NOT EXISTS """ + DB_TABLE_NAME + " (id INT(20) AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), price INT(20), new INT(1))"
cursor.execute(sql)

mydb.close()

# Scheduler
while True:
    exec(open('scraper.py').read())
    time.sleep(int(SLEEP_INTERVAL))

