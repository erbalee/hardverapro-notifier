import requests
import html
import os
from bs4 import BeautifulSoup
import datetime
import numpy as np
import mysql.connector
import modules.telegram as telegram

# DB
mydb = mysql.connector.connect(
  host=os.getenv("DB_HOST"),
  user=os.getenv("DB_USER"),
  password=os.getenv("DB_PASS"),
  database=os.getenv("DB_NAME")
)

cursor = mydb.cursor()

# Variables

URL = os.getenv("URL")
ITEM = os.getenv("ITEM")
TOKEN = os.getenv("TOKEN")
CHATID = os.getenv("CHATID")
DB_TABLE_NAME = os.getenv("DB_TABLE_NAME")
TOR_CONTAINER_NAME = os.getenv("TOR_CONTAINER_NAME")
TOR_PORT = os.getenv("TOR_PORT")
TOR_PROXY = "socks5://@" + TOR_CONTAINER_NAME + ":" + TOR_PORT
# Log
now = datetime.datetime.now()
print ("------------------------------------------------")
print ("Current datetime : ", now.strftime("%Y-%m-%d %H:%M:%S"))

# Scraper

response = requests.get(URL, 
                    proxies=dict(http=TOR_PROXY,
                                 https=TOR_PROXY))

# response = requests.get(URL)
website_html = response.text
print("Response code: ", response.status_code)

if(response.status_code == 200):
    soup = BeautifulSoup(website_html, "html.parser")
    all_items = soup.find_all(class_="uad-title")
    all_items_price = soup.find_all(name="div", class_="uad-price")
    # /Scraper

    # Finding only "a" tag values and put into array
    item_names = [item.find('a').text for item in all_items]

    # Format Price into number format (Removing currency, spaces etc.)
    item_prices = [itemprice.getText().strip().replace("Ft","").replace("Keresem","").replace(" ","")  for itemprice in all_items_price]

    # Merge 2 array into 1 matrix
    itemandprices=np.column_stack((item_names,item_prices))

    # Get all DB entry's name variable
    cursor.execute("""SELECT name FROM """ + DB_TABLE_NAME)

    db_item_names = [ x[0] for x in cursor.fetchall()]

    #Log
    print("Items found on the site:\n", item_names)


    #Removing items which are not in the current listing anymore
    for x in db_item_names:
        if x not in item_names:
            sql= """DELETE FROM """ + DB_TABLE_NAME + " WHERE name=%s"
            val=(x,)
            cursor.execute(sql,val)
            mydb.commit()
            print(x, " removed from the DB as it's no longer available")


    #Inserting new lines into DB, Set new status to 0 if the item was already in the previous listing.
    isthereanynew=0
    newitems=[]
    for x in itemandprices:
        if x[1] != "":
            if x[0] in db_item_names:
                sql= """UPDATE """ + DB_TABLE_NAME + " SET new = 0 WHERE name=%s"
                val=(x[0].strip(),)
                cursor.execute(sql,val)
                mydb.commit()
            else:
                print("New item found on the site: ", x[0])
                isthereanynew=1
                sql= """INSERT INTO """ + DB_TABLE_NAME + " (name,price,new) VALUES (%s,%s,%s)"
                val=(x[0].strip(),x[1].strip(),"1")
                cursor.execute(sql,val)
                mydb.commit()
                newitems.append(x)


    #If new item is on the listing pushing notification to telegram.
    if isthereanynew == 1:
      #Debug  print(newitems)
        message="New {} added [Check it]({})\nList:\n".format(ITEM, URL)
        for a in newitems:
            message+=a[0]+" - "+a[1]+"\n"
        print("Sending telegram notification")
        telegram.push(TOKEN, CHATID, message)

    #Log
    print ("Current datetime : ", now.strftime("%Y-%m-%d %H:%M:%S"))
    print ("----------------- Sleeping for:", os.getenv("SLEEP_INTERVAL"), "seconds -----------------------")
else:
    print ("Current datetime : ", now.strftime("%Y-%m-%d %H:%M:%S"), ": Hardverapro offlany... Response code: ", response.status_code)
